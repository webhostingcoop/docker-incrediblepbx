build: .built-centos .built-trusty

.built-centos:
	docker build \
		-f Dockerfile.centos \
		-t webhostingcoopteam/incrediblepbx:centos \
		.
	date -I > .built-centos

.built-trusty:
	docker build \
		-f Dockerfile.trusty \
		-t webhostingcoopteam/incrediblepbx:trusty \
		.
	date -I > .built-trusty
